from django.shortcuts import render
from django.views.generic import TemplateView

class HomeView(TemplateView):
    template_name = 'home/home.html'

    def dispatch(self, request, *args, **kwargs):
        return super(HomeView, self).dispatch(request)

class TermsView(TemplateView):
    template_name = 'terms/terms.html'

    def dispatch(self, request, *args, **kwargs):
        return super(TermsView, self).dispatch(request)