from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from countries_plus.models import Country

from .serializers import CountrySerializer


class CountryViewset(viewsets.ReadOnlyModelViewSet):
    model = Country
    serializer_class = CountrySerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return self.model.objects.all()
