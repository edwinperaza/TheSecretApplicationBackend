from django.db import models
from django.utils.translation import ugettext_lazy as _

from base.models import BaseModel

from .managers import TipQuerySet


class Tip(BaseModel):
    title = models.CharField(
        _('title'),
        max_length=100,
    )
    description = models.TextField(
        _('description'),
    )
    until_date = models.DateField(
        _('until date'),
    )
    language = models.ForeignKey(
        'languages.Language',
        related_name='tips',
        verbose_name=_('language'),
        on_delete=models.SET_NULL,
        null=True,
    )
    is_active = models.BooleanField(
        _('is active'),
        default=True,
        db_index=True,
    )

    objects = TipQuerySet.as_manager()

    def __str__(self):
        return self.title
