from django.utils import timezone

from base.managers import QuerySet


class TipQuerySet(QuerySet):

    def current(self):
        now = timezone.now()
        return self.filter(
            until_date__gte=now,
        )

    def active(self):
        return self.filter(
            is_active=True,
        ).current()
