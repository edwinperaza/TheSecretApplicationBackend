from django.contrib import admin

from .models import Tip


@admin.register(Tip)
class TipAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'until_date',
        'is_active',
        'language',
    )
    list_filter = (
        'is_active',
        'language',
    )
    search_fields = (
        'title',
        'description',
    )
