# Generated by Django 2.0.2 on 2018-02-18 23:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('languages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tip',
            fields=[
                ('id', models.AutoField(auto_created=True,
                                        primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(
                    auto_now_add=True, help_text='creation date')),
                ('updated_at', models.DateTimeField(
                    auto_now=True, help_text='edition date', null=True)),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('description', models.TextField(verbose_name='description')),
                ('until_date', models.DateField(verbose_name='until date')),
                ('is_active', models.BooleanField(
                    db_index=True, default=True, verbose_name='is active')),
                ('language', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL,
                                               related_name='tips', to='languages.Language', verbose_name='language')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
