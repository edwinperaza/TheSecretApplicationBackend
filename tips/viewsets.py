from rest_framework import viewsets

from .serializers import TipSerializer

from .models import Tip


class TipViewSet(viewsets.ReadOnlyModelViewSet):
    model = Tip
    serializer_class = TipSerializer
    filter_fields = ('language',)

    def get_queryset(self):
        return self.model.objects.active()
