from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from .models import Language

from .serializers import LanguageSerializer


class LanguageViewset(viewsets.ReadOnlyModelViewSet):
    model = Language
    serializer_class = LanguageSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return self.model.objects.all()
