from rest_framework import serializers

from .models import Objective


class ObjectiveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Objective
        fields = (
            'id',
            'user',
            'title',
            'description',
            'total_percentage',
            'until_date',
            'created_at',
        )

    def validate(self, attrs):
        instance = Objective(**attrs)
        instance.clean()
        return attrs
