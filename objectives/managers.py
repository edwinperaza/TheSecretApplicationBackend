from django.utils import timezone
from django.db.models import F

from base.managers import QuerySet


class ObjectiveQuerySet(QuerySet):

    def current(self, user):
        now = timezone.now()
        return self.filter(
            user=user,
            until_date__gte=now,
        )

    def get_current(self):
        now = timezone.now()
        return self.filter(
            user=F('user'),
            until_date__gte=now,
        )
