from django.contrib import admin

from .models import Objective


@admin.register(Objective)
class ObjectiveAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'user',
        'until_date',
    )
    list_filter = (
        'user',
    )
