import pandas

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.db.models.signals import post_save

from django.core.exceptions import ValidationError
from django.utils import timezone

from base.models import BaseModel

from .managers import ObjectiveQuerySet


class Objective(BaseModel):
    user = models.ForeignKey(
        'users.User',
        related_name='objectives',
        null=True,
        blank=False,
        verbose_name=_('user'),
        on_delete=models.SET_NULL,
    )
    title = models.CharField(
        _('title'),
        max_length=100,
    )
    description = models.TextField(
        _('description'),
    )
    until_date = models.DateField(
        _('until_date'),
    )

    objects = ObjectiveQuerySet.as_manager()

    def __str__(self):
        return self.title

    def _dates(self, start_date, until_date):
        dates = []
        dates_range = pandas.bdate_range(start_date, until_date)
        for date in dates_range:
            dates.append(date.date())
        return dates

    def total_percentage(self):
        '''
        return total percentage from last 10 days
        '''
        now = timezone.now().date()
        tomorrow = (
            now + timezone.timedelta(days=1)
        )
        days_with_exercises = (
            tomorrow -
            self.created_at.date()
        ).days

        if days_with_exercises >= 10:
            days_with_exercises = 10

        percentage_sum = sum([
            exercise.percentage()
            for exercise in self.exercises_by_week.filter(
                date__lte=now,
            ).order_by('-date')[:days_with_exercises]
        ])
        try:
            return (
                percentage_sum * 100 / days_with_exercises
            )
        except ZeroDivisionError:
            return 0

    def create_exercises_by_week(self):
        now = timezone.now()
        until_date = self.until_date

        dates = self._dates(now.date(), until_date)

        dates_by_week = {}
        for date in dates:
            week_number = date.isocalendar()[1]
            dates_by_week.setdefault(week_number, [])
            dates_by_week[week_number].append(date)

        from exercises.models import ExerciseByWeek
        from exercises.models import FaithExercise
        from exercises.models import AwarenessExercise

        language = self.user.language
        for week in dates_by_week.keys():
            dates = dates_by_week[week]

            faith_exercise = FaithExercise.objects.filter(
                language=language,
            ).order_by('?').first()

            awareness_exercise = AwarenessExercise.objects.filter(
                language=language,
            ).order_by('?').first()

            for date in dates:
                weekday = date.weekday()
                if weekday == 5 or weekday == 6:
                    continue

                ExerciseByWeek.objects.get_or_create(
                    objective=self,
                    date=date,
                    defaults={
                        'faith_exercise': faith_exercise,
                        'awareness_exercise': awareness_exercise,
                    }
                )

            ExerciseByWeek.objects.filter(
                date__gt=date, objective=self
            ).delete()

    def update_exercises_by_week(self):
        now = timezone.now()
        until_date = self.until_date

        dates = self._dates(now.date(), until_date)

        dates_by_week = {}
        for date in dates:
            week_number = date.isocalendar()[1]
            dates_by_week.setdefault(week_number, [])
            dates_by_week[week_number].append(date)

        from exercises.models import ExerciseByWeek
        from exercises.models import FaithExercise
        from exercises.models import AwarenessExercise

        language = self.user.language
        for week in dates_by_week.keys():
            dates = dates_by_week[week]

            faith_exercise = FaithExercise.objects.filter(
                language=language,
            ).order_by('?').first()

            awareness_exercise = AwarenessExercise.objects.filter(
                language=language,
            ).order_by('?').first()

            for date in dates:
                weekday = date.weekday()
                if weekday == 5 or weekday == 6:
                    continue

                ExerciseByWeek.objects.update_or_create(
                    objective=self,
                    date=date,
                    defaults={
                        'faith_exercise': faith_exercise,
                        'awareness_exercise': awareness_exercise,
                    }
                )

    def clean(self):
        if self.until_date <= timezone.now().date():
            message = _("You can not choose today's date or earlier")
            raise ValidationError({
                'until_date': message,
            })

        if not self.pk:
            current_object = Objective.objects.current(user=self.user)
            if self.until_date and current_object.exists():
                message = _(
                    'No se pueden agregar más objetivos mientras que haya alguno activo')
                raise ValidationError({
                    'until_date': message,
                })

    def save(self, *args, **kwargs):
        super(Objective, self).save(*args, **kwargs)


@receiver(post_save, sender=Objective, dispatch_uid='create_exercises_by_week')
def create_exercises_by_week(sender, instance, created, **kwargs):
    instance.create_exercises_by_week()
