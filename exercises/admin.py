from django.contrib import admin

from .models import FaithExercise
from .models import AwarenessExercise
from .models import ExerciseByWeek


class ExerciseAdmin(admin.ModelAdmin):
    exclude = (
        'kind',
    )
    list_display = (
        'id',
        'title',
        'language',
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(FaithExercise)
class FaithExerciseAdmin(ExerciseAdmin):
    pass


@admin.register(AwarenessExercise)
class AwarenessExerciseAdmin(ExerciseAdmin):
    exclude = ExerciseAdmin.exclude + (
        'to_show',
    )


@admin.register(ExerciseByWeek)
class ExerciseByWeekAdmin(admin.ModelAdmin):
    list_display = (
        'objective',
        'faith_exercise',
        'awareness_exercise',
        'date',
    )
    list_filter = (
        'objective',
    )

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related(
        ).prefetch_related(
            'objective',
            'faith_exercise',
            'awareness_exercise',
        )
        return queryset
