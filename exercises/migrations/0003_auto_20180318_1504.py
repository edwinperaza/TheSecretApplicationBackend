# Generated by Django 2.0.3 on 2018-03-18 18:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0002_exercisebyweek'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exercisebyweek',
            name='end_date',
        ),
        migrations.RemoveField(
            model_name='exercisebyweek',
            name='start_date',
        ),
        migrations.AddField(
            model_name='exercisebyweek',
            name='date',
            field=models.DateField(null=True, verbose_name='start date'),
        ),
        migrations.AddField(
            model_name='exercisebyweek',
            name='faith_exercise_its_done',
            field=models.BooleanField(default=False, verbose_name='faith exercise its done'),
        ),
    ]
