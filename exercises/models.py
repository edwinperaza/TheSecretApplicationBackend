from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from base.models import BaseModel

from .enums import ExerciseToShowEnum
from .enums import ExerciseKindEnum

from .managers import FaithExerciseManager
from .managers import AwarenessExerciseManager


class Exercise(BaseModel):
    kinds = ExerciseKindEnum
    to_shows = ExerciseToShowEnum

    title = models.CharField(
        _('title'),
        max_length=300,
    )
    description = models.TextField(
        _('description'),
    )
    language = models.ForeignKey(
        'languages.Language',
        verbose_name=_('language'),
        related_name='exercises',
        on_delete=models.SET_NULL,
        null=True,
    )
    to_show = models.CharField(
        _('to show'),
        max_length=50,
        choices=ExerciseToShowEnum.choices(),
        default=ExerciseToShowEnum.TITLE.value,
    )
    kind = models.PositiveSmallIntegerField(
        _('kind'),
        choices=ExerciseKindEnum.choices(),
        null=True,
    )

    def has_language(self, count):
        count_languages = self._meta.model.objects.filter(
            language=self.language).count()
        if count_languages >= count:
            raise ValidationError(
                {'language': _(
                    'you can not create more elements with the selected language.')}
            )
        return True

    def __str__(self):
        return self.title


class FaithExercise(Exercise):

    objects = FaithExerciseManager()

    class Meta:
        proxy = True

    def clean(self):
        if not self.pk:
            self.has_language(count=4)

    def save(self, *args, **kwargs):
        self.kind = ExerciseKindEnum.FAITH.value
        super().save(*args, **kwargs)


class AwarenessExercise(Exercise):

    objects = AwarenessExerciseManager()

    class Meta:
        proxy = True

    def clean(self):
        if not self.pk:
            self.has_language(count=5)

    def save(self, *args, **kwargs):
        self.kind = ExerciseKindEnum.AWARENESS.value
        super().save(*args, **kwargs)


class ExerciseByWeek(BaseModel):
    objective = models.ForeignKey(
        'objectives.Objective',
        verbose_name=_('objective'),
        related_name='exercises_by_week',
        null=True,
        on_delete=models.CASCADE,
    )
    faith_exercise = models.ForeignKey(
        FaithExercise,
        related_name='faith_exercise',
        verbose_name=_('faith exercise'),
        null=True,
        on_delete=models.SET_NULL,
    )
    faith_exercise_its_done = models.BooleanField(
        _('faith exercise its done'),
        default=False,
    )
    awareness_exercise = models.ForeignKey(
        AwarenessExercise,
        related_name='awareness_exercise',
        verbose_name=_('awareness exercise'),
        null=True,
        on_delete=models.SET_NULL,
    )
    awareness_exercise_its_done_one = models.BooleanField(
        _('awareness exercise its done one'),
        default=False,
    )
    awareness_exercise_its_done_two = models.BooleanField(
        _('awareness exercise its done two'),
        default=False,
    )
    awareness_exercise_its_done_three = models.BooleanField(
        _('awareness exercise its done three'),
        default=False,
    )
    awareness_exercise_its_done_four = models.BooleanField(
        _('awareness exercise its done four'),
        default=False,
    )
    awareness_exercise_its_done_five = models.BooleanField(
        _('awareness exercise its done five'),
        default=False,
    )
    love_exercise_its_done_one = models.BooleanField(
        _('love exercise its done one'),
        default=False,
    )
    love_exercise_its_done_two = models.BooleanField(
        _('love exercise its done two'),
        default=False,
    )
    date = models.DateField(
        _('date'),
        null=True,
    )

    def percentage(self):
        value = 0
        value += self.faith_exercise_its_done
        numbers = (
            'one',
            'two',
            'three',
            'four',
            'five',
        )
        awarness = []
        count = 0
        for number in numbers:
            field = '{}{}'.format(
                'awareness_exercise_its_done_',
                number,
            )
            has_value = getattr(self, field)
            if has_value:
                awarness.append(has_value)
                if number == 'one':
                    count = 1
                elif number == 'two':
                    count = 2
                elif number == 'three':
                    count = 3
                elif number == 'four':
                    count = 4
                elif number == 'five':
                    count = 5
                break

        #value += len(awarness) / 5
        value += count/5
        value += self.love_exercise_its_done_one
        #value += self.love_exercise_its_done_two
        return value / 3
