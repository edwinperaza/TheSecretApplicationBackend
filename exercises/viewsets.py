from django.utils import timezone

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from .serializers import ExerciseSerializer
from .serializers import ExerciseByWeekSerializer
from .serializers import ExerciseByWeekUpdateSerializer

from .models import Exercise
from .models import ExerciseByWeek


class ExerciseViewSet(viewsets.ReadOnlyModelViewSet):
    model = Exercise
    serializer_class = ExerciseSerializer
    filter_fields = ('language',)

    def get_queryset(self):
        return self.model.objects.filter()

    @list_route(methods=['get'])
    def current(self, request):

        date = request.GET.get('date')
        if not date:
            now = timezone.now()
            date = now

        exercise_by_week = ExerciseByWeek.objects.filter(
            objective__user=request.user,
            date=date,
        )
        if not exercise_by_week.exists():
            return Response({})

        serializer = ExerciseByWeekSerializer(exercise_by_week.first())
        return Response(serializer.data)

    def perform_update(self, serializer):
        return serializer.save()

    @list_route(methods=['put', 'patch'], url_path='current/update')
    def current_update(self, request, *args, **kwargs):
        date = request.GET.get('date')
        if not date:
            now = timezone.now()
            date = now
        else:
            now = date

        partial = kwargs.pop('partial', True)
        instance = ExerciseByWeek.objects.filter(
            objective__user=request.user,
            date=date,
        )
        if not instance.exists():
            return Response({'data': 'No data'})

        serializer = ExerciseByWeekUpdateSerializer(
            instance.first(), data=request.data, partial=partial)

        serializer.is_valid(raise_exception=True)
        obj = self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        serializer = ExerciseByWeekSerializer(obj)
        return Response(serializer.data)
