from rest_framework import serializers

from .models import Exercise
from .models import FaithExercise
from .models import AwarenessExercise
from .models import ExerciseByWeek

from objectives.serializers import ObjectiveSerializer


class ExerciseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Exercise
        fields = (
            'id',
            'title',
            'description',
            'language',
            'to_show',
            'kind',
            'updated_at',
        )


class FaithExerciseSerializer(serializers.ModelSerializer):

    class Meta:
        model = FaithExercise
        fields = (
            'id',
            'title',
            'description',
            'language',
            'to_show',
            'updated_at',
        )


class AwarenessExerciseSerializer(serializers.ModelSerializer):

    class Meta:
        model = AwarenessExercise
        fields = (
            'id',
            'title',
            'description',
            'language',
            'updated_at',
        )


class ExerciseByWeekSerializer(serializers.ModelSerializer):
    faith_exercise = FaithExerciseSerializer()
    awareness_exercise = AwarenessExerciseSerializer()
    objective = ObjectiveSerializer()

    class Meta:
        model = ExerciseByWeek
        fields = (
            'id',
            'objective',
            'faith_exercise',
            'faith_exercise_its_done',
            'awareness_exercise',
            'awareness_exercise_its_done_one',
            'awareness_exercise_its_done_two',
            'awareness_exercise_its_done_three',
            'awareness_exercise_its_done_four',
            'awareness_exercise_its_done_five',
            'love_exercise_its_done_one',
            'love_exercise_its_done_two',
            'percentage',
            'date',
        )


class ExerciseByWeekUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExerciseByWeek
        fields = (
            'faith_exercise_its_done',
            'awareness_exercise_its_done_one',
            'awareness_exercise_its_done_two',
            'awareness_exercise_its_done_three',
            'awareness_exercise_its_done_four',
            'awareness_exercise_its_done_five',
            'love_exercise_its_done_one',
            'love_exercise_its_done_two',
        )
