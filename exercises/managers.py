from base.managers import BaseManager

from .enums import ExerciseKindEnum


class FaithExerciseManager(BaseManager):

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(
            kind=ExerciseKindEnum.FAITH.value)


class AwarenessExerciseManager(BaseManager):

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(
            kind=ExerciseKindEnum.AWARENESS.value)
