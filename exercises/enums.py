from enum import Enum


class BaseEnum(Enum):

    @classmethod
    def choices(cls, is_int=True):
        choices = list(cls)
        return tuple(
            (value.value, value.name)
            for value in choices
        )

    @classmethod
    def get(cls, key, default=None):
        try:
            return cls[key]
        except:
            return default


class ExerciseToShowEnum(BaseEnum):
    TITLE = 'title'
    DESCRIPTION = 'description'


class ExerciseKindEnum(BaseEnum):
    FAITH = 1
    AWARENESS = 2
