from django.contrib.auth.forms import PasswordResetForm

from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework.decorators import list_route
from rest_framework import status

from push_notifications.models import GCMDevice

from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings

from .serializers import UserCreateSerializer
from .serializers import UserUpdateSerializer
from .models import User


jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserViewSet(
        viewsets.GenericViewSet,
        mixins.CreateModelMixin):
    model = User
    serializer_class = UserCreateSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        data = request.data.copy()

        email = data.get('email')
        if email:
            data['email'] = email.lower()

        user_serializer = self.get_serializer(data=data)
        user_serializer.is_valid(raise_exception=True)
        self.perform_create(user_serializer)
        headers = self.get_success_headers(user_serializer.data)
        serializer = JSONWebTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        user = serializer.object['user']
        del serializer.object['user']

        self.create_notification_data(data=data, user=user)

        serializer.object.update(
            user_serializer.data
        )
        return Response(
            serializer.object,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def create_notification_data(self, data, user=None):
        registration_id = data.get('registration_id')
        device_id = data.get('device_id')

        defaults = {
            'user': user,
            'device_id': device_id,
            'cloud_message_type': 'FCM',
        }
        GCMDevice.objects.update_or_create(
            registration_id=registration_id,
            defaults=defaults,
        )

    @list_route(methods=['post'], url_path='sign-in')
    def sign_in(self, request):
        data = request.data.copy()
        serializer = JSONWebTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        user_serializer = self.get_serializer(serializer.object['user'])
        user = serializer.object['user']
        del serializer.object['user']
        serializer.object.update(user_serializer.data)

        self.create_notification_data(data=data, user=user)

        return Response(
            serializer.object,
            content_type='application/json',
            status=status.HTTP_200_OK,
        )

    @list_route(methods=['post'], url_path='sign-out')
    def sign_out(self, request):
        return Response({}, status=status.HTTP_200_OK)

    @list_route(methods=['get'], permission_classes=(IsAuthenticated,))
    def current(self, request):
        serializer = self.get_serializer(self.request.user)
        return Response(serializer.data)

    @list_route(methods=['post'])
    def password(self, request):
        form = PasswordResetForm(request.data)
        if form.is_valid():
            form.save(request=request)
        return Response({}, status.HTTP_200_OK)

    @list_route(methods=['put', 'patch'], permission_classes=(IsAuthenticated,))
    def update_user(self, request, *args, **kwargs):
        instance = request.user
        serializer = UserUpdateSerializer(
            instance, data=request.data, partial=False)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    @list_route(methods=['post'])
    def installation(self, request):
        data = request.data.copy()
        user = None

        if request.user.pk:
            user = request.user

        self.create_notification_data(data=data, user=user)

        return Response({}, status.HTTP_200_OK)

    @list_route(methods=['post', 'get'], permission_classes=(IsAuthenticated,))
    def get_token(self, request):
        user = request.user
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return Response(
            {'token': token},
            status=status.HTTP_200_OK,
        )
